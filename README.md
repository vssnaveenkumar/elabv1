Elab V1.0

eLab is a computer-assisted student learning environment. eLab is a digitized platform that supports Flipped Classroom Model. 
This helps in independent practice work for the students even after the class hours. 
Within the classroom teacher extends his/her support and help in solving the problems. 
Flipping a classroom allows teachers to help busy and struggling students towards programming activities. 
This platform increases student-teacher and student- student interaction during class hours. 
Also gives teachers the time to provide more individualized differentiation, alleviates classroom management issues.


System Requirement:

	- Ubuntu 14.04
	- Apache2
	- mysql
	- php5.4
	- g++

Pre settings :
	- change apache port rather than PORT 80
	- Jail system run in PORT 80

Installation Steps:

	- Unzip “vpl-jail-system-2.1.1” run as root “install-vpl-sh” (./install-vpl-sh)
	- Unzip “Program, elabV1” in apache folder
	- Copy “codehere_data” inside eLabV1 and paste in “/var/”
	- Import “eLabV2.sql” to mysql database “codehere”

Configuring Steps:

In apache folder

	- program->config.php (mysql username and password, deployed location)
	- codehere->config.php (mysql username and password, deployed location)
	- code-here->config->database.php (mysql username and password)

Default Username and Password:

	- Username: admin
	- Password: Hari18@Naveen 

Server Maintanace:

Apache2 server

	- service apache2 start
	- service apache2 stop
	- service apache2 restart
	- service apache2 status

Vpl-jail server

	- service vpl-jail-system start
	- service vpl-jail-system stop
	- service vpl-jail-system restart
	- service vpl-jail-system status

### Developer Naveen Kumar S###

phone : +91 90476 90104

email : vssnaveenkumar@gmail.com

linkedin : https://in.linkedin.com/in/vssnaveenkumar


### eLab V2.0 ###

eLab V2.0 is highly efficient with support of 10+ programming Languages and with high performance. it has the ability to handle more parallel hits.

for more information regarding eLab V2.0 any one can contact me at any time 

Contact given above